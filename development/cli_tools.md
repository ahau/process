# Command line tools

## NPM cli

common commands to know
- `npm install`
- `npm link` + `npm link <package_name>`
- `npm version patch|minor|major`
- `npm publish`
- `npm outdated`
- `npm ls`
- `npm ls <package-name>`
- `npm update <package-name> --depth 5`

## ssb-server

```bash
$ npm i ssb-server -g
$ ssb_appname=ahau-dev ssb-server whoami
$ ssb_appname=ahau-dev ssb-server status
$ ssb_appname=ahau-dev ssb-server profile.findByFeedId <SomeFeedId>
```

Assuming your scuttlebutt backend is running you can use `ssb-server`'s command line
helper to query the currently running backend from the terminal.

NOTE: prefixing with e.g. `ssb_appname=ahau-pataka-dev` will point the CLI tool at the identity
based in the folder `~/.ahau-pataka-dev`

## dependency-check

https://www.npmjs.com/package/dependency-check

A module which can help you auto-check for missing dependencies before e.g. publishing

Install it globally:

```bash
$ npm i -g dependency-check
```

Then add the following to your .zshrc
```
npm () {
  ([ "$1" != "publish" ] || dependency-check . --no-dev) && command npm "$@"
}
// NOTE the --no-dev tells it to ignore checking devDependencies
```

## ripgrep (rg)

https://www.npmjs.com/package/dependency-check

An incredibly fast commandline search tool.

Example:

```bash
$ rg preferredName

ui/src/components/dialog/profile/EditNodeDialog.vue
39:    preferredName: profile.preferredName,

ui/src/components/dialog/profile/NewNodeDialog.vue
16:              v-model="formData.preferredName"
26:              :search-input.sync="formData.preferredName"
36:                    <Avatar class="mr-3" size="40px" :image="data.item.profile.avatarImage" :alt="data.item.profile.preferredName" :gender="data.item.profile.gender" :aliveInterval="data.item.profile.aliveInterval" />
38:                      <v-list-item-title> {{ data.item.profile.preferredName }} </v-list-item-title>
91:    preferredName: profile.preferredName,
113:    preferredName: '',
392:    'formData.preferredName' (newValue) {

// ...
// colouring not shown here
```

You can also use regex searchs (e.g. to widen to select for all names):
```bash
$ rg -e '(alt|preferred|legal)Name' 
```

If you want to tell it to only look in particular folder:
```bash
$ rg preferredName ui/src
```

By default ripgrep ignores your `node_modules` folder. If you need to go looking for the source of some bug, just point your query at that folder:
```bash
$ rg preferredName node_modules
```

