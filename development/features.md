# Building a new Feature

1. To see what needs to be done next:
  - Check the "Up next" column
  - Pull tasks from the backlog and move it into the "Up next" column
  - Feel free to split a sub-task into a different task
2. Move the current task you are working on into the active column and assign it to yourself
3. Create a git branch off of `master` for the task you are working on with a similar name to the asana card  
4. Create a merge/pull request for the branch you are currently working on, mark it as WIP (work in progress)
5. Keep the branch fresh!
  - regularly commit changes to the branch.
  - regularly pull from the `master` branch
  - push all changes up to gitlab, _especially at the end of a working day_
6. If you require any feedback before you can complete the task, use the Asana comments describing what you would like feedback on, assign the task to the person you would like feedback from and let that person know in rocketchat #development channel  
7. Submit the PR for review
  - remove the WIP status
  - use the description template to add a description of your PR and post the link to your PR in the #development channel for someone to [review](#reviews)
  - TODO _decide where Designer / Product Owner reviews work!_
8. Make changes
  - after someone has reviewed it, you may need to make some changes and have it reviewed again (loop to 7)
9. Merge
  - after everything is good, merge the work, deleting the branch from gitlab
10. Mark done in Asana



