## Reviews

There are several sorts of review that can be done. You need to decide which are relevant and name them in communication.

**Please keep in mind** that while you're reviewing another persons work, you're in a position of quite some power over them.
I highly recommend:
- celebrating sweet pieces of code (if you learn a new trick, or love how elegant it is)
- naming what's working as well as what isn't
- talking about things which feel good about the change
- acknowledging the work that's been done if it's big
- throw in some cute emoji :tropical_fish: :pineapple: or [kaomoji](http://japaneseemoticons.me/excited-emoticons/) ` (ﾉ´ヮ´)ﾉ*:･ﾟ✧ `

All of these things help make a review something you're doing _together_, and reminds you that there are people involved, and that criticism of the code that's
been written is not a criticism of a person.
Sweet vibes help us all go a lot further together.


### Smoke Test

_If you turn the machine on an push a button, does smoke come out of it?_

In this sort of test, a peer:
1. read the description of the piece of work in the PR
2. spins up your branch
3. click around to see if you can break it
  - try doing silly things (like 100 characters in a name)
  - poke parts of the app adjacent to this feature
  - see if it works if you start from scracth / start with an existing record
  - do the tests pass?
4. if you find anything unexpected
  - post detailed notes on the PR
  - take screenshots

It can be helpful to create a clear checklist of things you tried like 

```
- [ ] added a parent to the top of the graph
- [x] added a parent to a person lower in the graph
- [x] added a child
```


### Code Review

The purpose of this sort of review is to ensure the code is safe to merge.
This is measured on several axes:
- **danger** :fire:
  - is this code going to hurt anyone or their data if it's run ?
  - is it going to be incredibly resource intensive
- **technical debt**
  - if someone needs to fix / modify this in the future, is it going to be easy to understand?
  - if something _complex_ (e.g. there are many paths through it) was added, is there a test around it?
  - if a beginner copies a pattern they've seen here, is the app going to become a mess?
  - is this adding repetition to the point that it will make maintenance hard (note: copying code 2-3 times can be appropriate)
  - if there's something messy, is it clearly commented as such, with all the info needed to improve the code in the future?
- **style**
  - is the style consistent with the rest of the app (this sounds strange, but coherent style mean predicatable patterns which ease comprehension and reduce errors)
  - as an application grows, our style will evolve (same if new people join). Let's keep talking about it so that we're writing code that serves us well (i.t.o. comprehension)

_If you don't know what technical debt is yet, it's imperative we talk about it!_


Steps for a code review:
1. read the description of the piece of work in the PR
2. have a high-level look at the files changed
  - any surprises?
  - which files are core to this PR, versus periforal?
3. read through the files, this can look like:
  - look for silly mistakes (leftover comments, TODOs, console.logs)
  - follow the flow of logic
    - this can be expensive i.t.o. time, but it can lead to a deeper understanding of the app, which pays off later
    - you don't need to understand everything 100%
  - look for confusing code
    - see if reading it a little longer helps
    - is that function doing too much? 
    - is this the right place for this functionality
    - are there unhelpful variable names like `_v`, `whakapapa`
4. leave inline comments
  - clearly mark what sort of comment it is (:fire: = danger!, "style" = let's talk together, this is non-vital. Everything else is just a suggestion)
  - remember to celebrate successes, and leave cute emoji / [kaomoji](http://japaneseemoticons.me/excited-emoticons/)
  - if there's nothing to critique, skip to _merge_
5. hand the review to the author
  - direct-message / @-mention the author with a link to the PR
6. do follow up review
  - you're on reviewing this branch until it's merged
7. merge
  - make sure you check someone has smoke-tested this before you merge. If no-one has, do this yourself now
  - select "delete branch"
  - decide whether to select "squash commits"

#### Making changes as the reviewer

It's best to leave changes in the hands of the author, but sometimes this isn't practical 
e.g. it's a linting fixup, it's moving a file without changing the functionality, the person is on holiday

If you really do need to make a change consider:
- do you _really_ need to?
  - messing with a persons branch can be really annoying e.g. they might be offline working on it
  - is this thing blocking something else? if not, leave it

- making a branch off of this one, and opening a PR to the original branch
  - do this _even if you plan to merge it yourself_
  - this gives you a place to point to and talk about your changes
  - this helps to be able to "show" a person the change you meant without having to do it everywhere. this sort of branch might even be thrown out later

- if it turns into a big change, you may need to take off your reviewer hat, and get someone else to review the final work


### Design / UX / Product Owner review

TODO


