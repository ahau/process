# Dev Setup

See the README of particular repos for setup requirements.

Our general setups are:

## Code Style

### Editor tab / spaces

The `tab size` should be set to `2 space`.
That is our code doesn't inlucde `\t`, instead you should find indents are `\s\s`.

Make sure you figure out how to set this as your default in your code-editor of choice


### Linting

Make sure you have automatic linting set up in your editor.

**Option 1:**

- Go to: https://standardjs.com/
- VS Code:
    - https://github.com/standard/vscode-standardjs
    - Search the page for a mention of `vue` and it has options for getting it going

**Option 2:** (What Cherese uses for VS Code if the above doesnt work)

- Download these extensions:
    - ESLint - Dirk Baeumer
    - Vetur - Pine Wu
- Press `cmd ,` which will take you to settings
- Search `eslint.validate`
- Under `Eslint: Validate`, press the `Edit in settings.json`
- Check the following are included in there:
  ```
  {
    // ...
    "eslint.autoFixOnSave": false,
    "eslint.validate": [
      { "language": "vue", "autoFix": true }, 
      { "language": "html", "autoFix": true },
      { "language": "javascript", "autoFix": true }, 
      { "language": "typescript", "autoFix": true }
    ]
  }
  ```

> You may need to reset VS Code for changes to be applied
