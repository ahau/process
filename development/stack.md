# Tutorials

Here's a collection of resources which are relevant to learning our stack

## Application architecture

Ahau is an electron-app which runs the following processes:
- **main** - the default electron process where we coordinates startup of the other processes
- **background** - this runs "backend" processes, i.e. Scuttlebutt + GraphQL API
- **ui** - this runs Vue, which fetches data from the GraphQL API

Most of the electron startup and management is handled by a module called `ssb-ahoy`.
This module does things like check the background processes are all go before launching the UI.

## UI
The UI (user interface) / front-end is built with Vue.  When the UI starts it checks to ensure the GraphQL API is ready then starts offering the user options.
Database queries and mutations are sent via GraphQL requests to the backend.
This is either done in components or sometimes in Vuex (redux like state machine for Vue)

During development we use a "development server" which dynamically rebuilds the ui bundle and sereves that up
in a hot-reloading way (managed by Vue and webpack).

When it's time to build installers we run a different command which bundles up and optimises all the JS,
templates, CSS, into static files.

**Need to know**
- Vue basics
- Vuetify basics (Material UI component framework)
- How to do GraphQL queries from components (using apollo)
- How to explore the GraphQL API (using graphql : http://localhost:4000/graphql)

**Advanced**
- Testing:
  - use `tape` to write unit tests for any 'complex' data manipulation
  - use `storybook` to test isolated components with mock data
- D3 for visualisation in the whakapapa tree


### UI Resources

- Vue docs
- Vuetify
- Apollo vue docs
- tape docs
TODO ^ LINK UP

[![](http://img.youtube.com/vi/tVNa6_tFD70/0.jpg)](http://www.youtube.com/watch?v=tVNa6_tFD70 "Using Storybook in Ahau - Simple Tutorial")
_Using Storybook in Ahau - Simple Tutorial_

[![](http://i3.ytimg.com/vi/x3kzCg-kOT8/hqdefault.jpg)](https://youtu.be/x3kzCg-kOT8)
_Making the `ViewEditNodeDialog` responsive_
- Setting up a Storybook story for mobile-first and responsive development
- Using the existing responsive `Dialog` component for mobile dialogs
- Using the exposed `$vuetify.breakpoint` to determine if it's mobile screen
- Using the Vuetify grid system to adapt a component to be reponsive and mobile first

## Backend

The Backend is built with GraphQL and Secure Scuttlebutt (SSB).
GraphQL provides a nice API with super *sugary* helpful methods specific to our context.
Secure Scuttlebutt handles:
- database methods as minimal as possible and tightly tested
- replicating data with other peers

In terms of setup the backend has a hierarchy of modules something like:

```
secret-stack   // the extensible core of SSB                         }
├── ssb-db        // adds SSB database                               }
├── ssb-query     // adds an easy to query index to ssb-db           }--basic SSB
├── ssb-conn      // manages starting connections with other peers   }
├── ssb-replicate // manages which other feeds you request           }
├── ...
│
├── ssb-tribes     // adds group encryption                                                       }
├── ssb-profile    // adds profile methods to db                                                  }
├── ssb-whakapapa  // adds methods for linking e.g. profiles to db                                }
├── ...                                                                                           }--ahau
└── ssb-ahau       // adds a GraphQL server (api), built from:                                    }
      ├── @ssb-graphql/main      // basic whoami + fileUpload API                                 }
      ├── @ssb-graphql/profile   // gives access to ssb-profile + add higher level methods        }
      ├── @ssb-graphql/whakapapa // gives access to ssb-whakapapa + adds higher level methods     }
      └── - ...
```

NOTES:
- modules starting with `ssb-*` are _generally_ "ssb plugins" - these are modules which extend `secret-stack`
  - the "basic" scuttelbutt modules are common to most apps and can be found on github orgs ([SSBC](github.com/ssbc) or [ssb-js](github.com/ssb-js))
  - `ssb-tribes` is core to Ahau, but was built to be used generally
- modules starting with `@ssb-graphql/*` are all modules which are parts which can be combined to make a GraphQL API.
- modules in the _basic SSB_ are modules which are used in basically all Node.js SSB projects
- modules in the _ahau_ are modules are more unique to ahau

**Need to know**
- Testing
  - testing for ssb-plugins which provide database methods is non-negotiable
  - we use `tape` mostly - simple and powerful
- scuttlebutt
  - plugins: `{ name, manifest, init }`
  - commonly used APIs: `ssb-query`, `ssb-backlinks`
  - pull-streams
- GraphQL basics
  - typeDefs, resolvers
  - graphql playground

**Advanced**
- Testing GraphQL
- caching queries (roll your own, or use apollo)
- ...


### Backend Resources


[![](http://i3.ytimg.com/vi/JBMCCHOrrYs/hqdefault.jpg)](https://www.youtube.com/watch?v=JBMCCHOrrYs)
_adding a new field `ignoredProfiles` to `ssb-whakapapa`_
- updating the schema, strategy, methods
- adding tests
- NOTE: this is quite a hard case because we were updating a "set type" field, as opposed to an "overwrite type" field


