
# Deployment 

## Ahau Desktop

This guide outlines the steps to update the application version, create new installers and to deploy to the protozoa github releases page

1. **make sure you have the code signing password installed** (for Windows/ Mac)
    - to get a copy you will need to contact Ben/ Mix/ Cherese and ask for the `electron-builder.env` files or credentials
    - then we want to make we are in the master branch (`git checkout master`)
    - this file than needs to be added to `/desktop` or `/pataka` depending on if you are building Ahau or Pataka. (It should sit next to `electron-builder.env.template`)


2. **we update the application version number**
    - first get the latest version number which will be at the bottom of the list
        - `git tag -l`
    - in `desktop/package.json` update the version number to the version number that we want to upgrade to eg: 1.2.0
    - then in `/desktop` run `npm install` to update the version in the package-lock.json
    - Then in the root folder run `npm version [major|minor|patch]` to update the version number to the same version number used earlier 
    - then want to push these changes up to gitlab + github
        - `git push origin master --tags`

(check everything looks right with `git log`, looking for the commit that was made and the tag)

3. **build installers**
    - from the root folder run `npm run release:desktop` or `pataka` depending on what app your building 

4. **push it to protozoa releases**
    - this is just a github repo which allows us to host the installer files easily
    - `git push github master --tags`
    - if you don't have github set up as a remote add it with
        - `git remote add github ......` (fill in the blanks with https/ ssh repo address)

5. **create a new "release"**
    - go to `https://github.com/protozoa-nz/whakapapa-ora/releases` and "click draft new release"
    - start typing the the version number in teh input field and change the `@target` to `master`
    - add some information like  `new features`, `bug fixes` and `known issues`
    - attach your .exe / .dmg / .AppImage installer files
    - click `publish release`

Let everyone know on the dev chat and maybe ask someone to update the download links in docs 
