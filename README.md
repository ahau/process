# Mātou Collective Processes

Nau mai haere mai, welcome to Mātou team documentation.
If you are interested in contributing to this project or would like to learn more please feel free to get in contact info@ahau.io 

## Developers

- [onboarding](./onboarding.md) - you're new to the team, here's what we need to get set up!
- development
  - [setup](./development/setup.md)
  - [stack](./development/stack.md) - an overview of the stack with links to tutorials
  - features
    - [building a new feature](./development/features.md)
    - [MR template](./development/mr_template.md)    
    - [reviews](./development/reviews.md)
  - [cli tools](./development/cli_tools.md) - some tools and tips and tricks you might use
  - [debugging](./development/debugging.md) - helpful things to know when debugging this stack

## General

- [karakia](./karakia.md)
- [te reo](./te-reo.md)
- [invoicing](./invoice.md)

