# Onboarding

If you're new to the team, you need to know where things are, how and where we do comms etc.

WIP

## How we work

- we currently open weekly meetings with:
  - a karakia - a prayer which invokes things we want to keep in mind as we do this work
  - a check in - share how things are going in our lives (outside of code!)

- we take turns leading meetings


## Communication Tools

- **regular comms**
  - chat.ahau.io we use this RocketChat for day-to-day comms
- **urgent direct contact**
  - Signal (you'll need people's cell phone numbers
- **synchronous meetings** (use depends on needs)
  - audio/ video
    - mumble (needs installing, ask how + for config)
    - talky.io/ahau
    - meet.jit.si/ahau
    - zoom - this is used in stakeholder hui (needs installing)
  - notes
    - hackmd.io/new


## Assets

- **git orgs**
  - gitlab.com/ahau (most repos live here)
  - github.com/ssbc
- **design material**
  - google drive
  - ???
- **grant writing**
  - google drive


