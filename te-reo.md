# Papakupu Te-reo - Māori dictionary 
A list of words an phrases that our team can incorporate into our te-reo māori learning and application   

### Greetings

| Word          | Meaning           |
| ------------- |:-------------:|
| kia ora      | hello/hi (informal) / wishing someone life and health |
| morena      | good morning      |
| e hoa | friend      |
| e hoa ma | friends      |
| ngā mihi | thanks      |

### Supporting

| Word          | Meaning           |
| ------------- |:-------------:|
| Karawhiua     | give it a go! |
| kia mau     | hang in there |
| ka taea e koe!     | you can do it |
| kia ū     | all the best |
| māku e āwhina     | i'll help |
| kaua e wareware     | dont forget |
| kia tūpato!     | be careful |

### Expressing surprise
| Word          | Meaning           |
| ------------- |:-------------:|
| E kī!     | well, well |
| Āe marika     | how about that! |
| Auē!     | oh no! |

### Congratulating 
| Word          | Meaning           |
| ------------- |:-------------:|
| Ka rawe     | awesome |
| Tino pai     | great stuff |
| Ātaahua!     | beautiful |

### Commesirating
| Word          | Meaning           |
| ------------- |:-------------:|
| Ka aroha hoki!     | you poor things |
| Aroha mai!     | im so sorry |
