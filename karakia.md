# Mātou karakia

The different karakia that Mātou team will use for hui

## Karakia Whakatuwhera + Whakakapi (To open or close a meeting)

```
Unuhia te pō, te pō whiri mārama
Tomokia te ao, te ao whatu tāngata
Tātai ki runga, tātai ki raro, tātai aho rau
Haumi e, hui e, tāiki e!
```
```
From the confusion comes understanding
From understanding comes unity
We are interwoven, we are interconnected
Together as one!
```

## Karakia Timatanga (To open a meeting)

```
Kia tau ngā manaakitanga a te mea ngaro
ki runga ki tēnā, ki tēnā o tātou
Kia mahea te hua mākihikihi
kia toi te kupu, toi te mana, toi te aroha, toi te Reo Māori
kia tūturu, ka whakamaua kia tīna! Tīna!
Hui e, Tāiki e!
```
```
Let the strength and life force of our ancestors
Be with each and every one of us
Freeing our path from obstruction
So that our words, spiritual power, love, and language are upheld;
Permanently fixed, established and understood!
Forward together!
```

## Karakia Whakamutunga (To close a meeting)
```
Kia whakairia te tapu
Kia wātea ai te ara
Kia turuki whakataha ai
Kia turuki whakataha ai
Haumi e. Hui e. Tāiki e!
```
```
Restrictions are moved aside
So the pathways is clear
To return to everyday activities
Gather, united, forward together
```
# Āhau application karakia

## For developers 
### when opening server
```
He Karakia Tīmatanga me te Whakakapi Kaupapa
Kia tau ngā manaakitanga a te mea ngaro
ki runga ki tēnā, ki tēnā o tātou
Kia mahea te hua mākihikihi
kia toi te kupu, toi te mana, toi te aroha, toi te Reo Māori
kia tūturu, ka whakamaua kia tīna! Tīna!
Hui e, Tāiki e!
```
```
Let the strength and life force of our ancestors
Be with each and every one of us
Freeing our path from obstruction
So that our words, spiritual power, love, and language are upheld;
Permanently fixed, established and understood!
Forward together!
```
## For people
### Signing in 
```
E te tangata
Whāia te māutauranga kai mārama
Kia whai take ngā mahi katoa
Tū māia, tū kaha
Aroha atu, aroha mai 
Tātou i a tātou katoa
```
```
For this person
Seek knowledge for understanding
Have purpose in all that you do
Stand tall, be strong
Lets us all show respect
For each other
```
### Signing out 
```
Kia whakairia te tapu
Kia wātea ai te ara
Kia turuki whakataha ai
Kia turuki whakataha ai
Haumi e. Hui e. Tāiki e!
```
```
Restrictions are moved aside
So the pathways is clear
To return to everyday activities
Gather, united, forward together
```
